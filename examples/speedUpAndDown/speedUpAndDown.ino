#include "SerialElmo.h"

// the library assumes that the PC is connected to the default serial port at 9600bps
// and that the ELMO is cnnected to Serial1 at 115200bps

// create ELMO and initiate seral connections
SerialElmo elmo = SerialElmo();

void setup() {
    
}

void loop() {
  elmo.enable();
  delay(3000);

  //speed up from 0 to 28000 [cnt/sec] 
  for (int x=1; x<15; x++){
    elmo.jogging(-2000*x);
    delay(500);
  }

  //speed down from 30000 to 0 [cnt/sec] 
  for (int x=15; x>1; x--){
    elmo.jogging(-2000*x);
    delay(500);
  }
  
  elmo.disable();
  delay(3000);
}
