#include "Arduino.h"
#include "SerialElmo.h"

SerialElmo::SerialElmo(){}

// Enable Elmo by sending the Command "MO=1" and print PC_SERIAL Buffer
void SerialElmo::enable(){
  PC_SERIAL.println("Send MO=1");
  // to Elmo
  flushInputBuffer();
  ELMO_SERIAL.print("MO=1");
  ELMO_SERIAL.print("\r");
  delay(100);
  printResponse();
}

// Disable Elmo by sending the Command "MO=0" and print PC_SERIAL Buffer
void SerialElmo::disable(){
  PC_SERIAL.println("Send MO=0");
  // to Elmo
  flushInputBuffer();
  ELMO_SERIAL.print("MO=0");
  ELMO_SERIAL.print("\r");
  delay(100);
  printResponse();
}

// Turn Elmo in Jogging mode by setting the speed with "JV" command and sending the command "BG"
// set positive value to rotate to the right, negative value to rotate to the left
void SerialElmo::jogging (int speed){
	
  PC_SERIAL.println("Send JV");
  ELMO_SERIAL.print("JV=");
  ELMO_SERIAL.print(speed);
  ELMO_SERIAL.print("\r");
  delay(RESPONSEWAIT_MS);
  printResponse();
 
  PC_SERIAL.println("Send BG");
  ELMO_SERIAL.print("BG");
  ELMO_SERIAL.print("\r");
  delay(RESPONSEWAIT_MS);
  printResponse();
}

void SerialElmo::flushInputBuffer(){
  while(ELMO_SERIAL.available() > 0) {
    ELMO_SERIAL.read();
  }
}  

void SerialElmo::printResponse(){
	 while (ELMO_SERIAL.available() !=0 ){
    PC_SERIAL.write(ELMO_SERIAL.read());
  } 
}