// A library to communicate with an ELMO Gold Drive over the Serial SimplIQ interface
// 2016

// the library assumes that the PC is connected to the default serial port at 9600bps
// and that the ELMO is cnnected to Serial1 at 115200bps

#include "Arduino.h"

#define ELMO_SERIAL Serial1
#define PC_SERIAL Serial

#define RESPONSEWAIT_MS 100

#ifndef SerialElmo_h
#define SerialElmo_h

class SerialElmo 
{
	public:
		SerialElmo();
		void enable();
		void disable();
		void jogging(int speed);
		
	
	private:
		void flushInputBuffer();
		void printResponse();	
};

#endif
